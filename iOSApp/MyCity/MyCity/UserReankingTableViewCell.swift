//
//  UserReankingTableViewCell.swift
//  MyCity
//
//  Created by Alexandre Martinez Olmos on 17/6/15.
//  Copyright (c) 2015 BKPAMO. All rights reserved.
//

import UIKit

class UserReankingTableViewCell: UITableViewCell {
    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var userLabel: UILabel!
    @IBOutlet weak var pointsLabel: UILabel!
    @IBOutlet weak var rankingImage: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
