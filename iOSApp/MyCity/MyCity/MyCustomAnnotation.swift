//
//  MyCustomAnnotation.swift
//  MyCity
//
//  Created by Alexandre Martinez Olmos on 16/6/15.
//  Copyright (c) 2015 BKPAMO. All rights reserved.
//

import UIKit
import MapKit

class MyCustomAnnotation: NSObject, MKAnnotation {
    var title: String!
    var subtitle: String!
    var type: Int
    var icon: UIImage
    var coordinate: CLLocationCoordinate2D
    
    var user: String!
    var descript: String!
    var date: String!
    var town: String!
    var status: Int
    
    func getTitle() -> NSString{
        return self.title
    }
    
    func getSubtitle () -> NSString{
        return self.subtitle
    }
    
    func getType () -> Int{
        return self.type
    }
    
    init(c: CLLocationCoordinate2D, t: String!, st: String!, ty: Int, i: UIImage, u: String!, d: String!, dat: String!, tow: String!, s: Int){
        coordinate = c
        title = t as String
        subtitle = st as String
        icon = i
        type = ty
        user = u
        descript = d
        date = dat
        town = tow
        status = s
    }
}
