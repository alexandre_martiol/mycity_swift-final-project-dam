//
//  SlideMenuViewController.swift
//  MyCity
//
//  Created by AlexM on 13/04/15.
//  Copyright (c) 2015 BKPAMO. All rights reserved.
//

import UIKit
import Alamofire

class SlideMenuViewController: UIViewController {
    @IBOutlet weak var userImage: UIImageView!
    @IBOutlet weak var usernameLabel: UILabel!
    @IBOutlet weak var pointsLabel: UILabel!
    @IBOutlet weak var numberIncidencesLabel: UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()

        //Code to make the imageView like a circle
        userImage.layer.borderWidth=1.0
        userImage.layer.masksToBounds = false
        userImage.layer.borderColor = UIColor.blackColor().CGColor
        userImage.layer.cornerRadius = 13
        userImage.layer.cornerRadius = userImage.frame.size.height/2
        userImage.clipsToBounds = true
        
        var baseURL = "http://localhost:8080/user?username=\(userLogged)"
        
        Alamofire.request(.GET, baseURL)
            .responseJSON { (request, response, data, error) in
                println("DATA OF THE USER: \(data)")
                
                if (data != nil) {
                    var dic: NSDictionary! = data as! NSDictionary
                    
                    if (dic.count > 0 && error == nil) {
                        var id: AnyObject! = data!.valueForKey("id")
                        var username: AnyObject! = data!.valueForKey("username")
                        var points: AnyObject! = data!.valueForKey("points")
                        
                        var incidences = data!.valueForKey("incidences") as! NSArray
                        var incidencesCount = incidences.count
                        
                        self.usernameLabel.text = "\(username)"
                        self.pointsLabel.text = "Puntuación: \(points)"
                        self.numberIncidencesLabel.text = "Incidencias Reportadas: \(incidencesCount)"
                    }
                }
        }
    }
    
    @IBAction func incidencesTableAction(sender: AnyObject) {
        var storyboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        var vc : IncidencesTableViewController = storyboard.instantiateViewControllerWithIdentifier("IncidencesTableView") as! IncidencesTableViewController
        
        self.presentViewController(vc, animated: true, completion: nil)
    }
    
    
    @IBAction func myIncidencesTableAction(sender: AnyObject) {
        var storyboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        var vc : IncidencesTableViewController = storyboard.instantiateViewControllerWithIdentifier("IncidencesTableView") as! IncidencesTableViewController
        
        vc.isCurrentUserRequest = true
        
        self.presentViewController(vc, animated: true, completion: nil)
    }
    
    @IBAction func logOutAction(sender: AnyObject) {
        //Destroy the present session and delete the credentials in keychain
        //...
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewControllerWithIdentifier("LoginViewController") as! LoginViewController
        self.presentViewController(vc, animated: true, completion: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
