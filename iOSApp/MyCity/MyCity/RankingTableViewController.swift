//
//  RankingTableViewController.swift
//  MyCity
//
//  Created by Alexandre Martinez Olmos on 17/6/15.
//  Copyright (c) 2015 BKPAMO. All rights reserved.
//

import UIKit
import Alamofire

class RankingTableViewController: UITableViewController {
    @IBOutlet var table: UITableView!
    var UsersArray: [NSDictionary] = []

    override func viewDidLoad() {
        super.viewDidLoad()

        self.refreshTable()
    }
    
    func refreshTable() {
        var baseURL = "http://localhost:8080/users?sort=points,desc"
        
        Alamofire.request(.GET, baseURL)
            .responseJSON { (request, response, data, error) in
                println("DATA OF THE USERS: \(data)")
                
                if (data != nil) {
                    var dic: NSDictionary! = data as! NSDictionary
                    
                    if (dic.count > 0 && error == nil) {
                        var embedded  = data!.valueForKey("_embedded") as! NSDictionary
                        self.UsersArray = embedded.valueForKey("users") as! [NSDictionary]
                        
                        dispatch_async(dispatch_get_main_queue(), { () -> Void in
                            self.table.reloadData()
                        })
                    }
                }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Potentially incomplete method implementation.
        // Return the number of sections.
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete method implementation.
        // Return the number of rows in the section.
        return UsersArray.count
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell: UserReankingTableViewCell = tableView.dequeueReusableCellWithIdentifier("userCell") as! UserReankingTableViewCell!
        
        // Configure the cell...
        var currentUser = UsersArray[indexPath.row] as NSDictionary
        var username: AnyObject! = currentUser["username"]
        var points: AnyObject! = currentUser["points"]
        
        if (indexPath.row == 0) {
            cell.rankingImage.image = UIImage(named: "firstPlace")
        }
        
        else if (indexPath.row == 1) {
            cell.rankingImage.image = UIImage(named: "secondPlace")
        }
        
        else if (indexPath.row == 2) {
            cell.rankingImage.image = UIImage(named: "thirdPlace")
        }
        
        cell.userLabel.text = "\(username)"
        cell.pointsLabel.text = "Puntos: \(points)"
        
        return cell
    }
}
