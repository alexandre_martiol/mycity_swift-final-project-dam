//
//  User.swift
//  MyCity
//
//  Created by Alexandre Martinez Olmos on 16/4/15.
//  Copyright (c) 2015 BKPAMO. All rights reserved.
//

import UIKit

class User: NSObject {
    var id: Int
    var username: String
    var name: String
    var lastname: String
    var password: String
    var enabled: Int
    var email: String
    var sex: Int
    var points: Int
    var incidences: [Incidence]
    
    init(id: Int, username: String, name: String, lastname: String, password: String, enabled: Int, email: String, sex: Int, points: Int, incidences: [Incidence]) {
        self.id = id
        self.username = username
        self.name = name
        self.lastname = lastname
        self.password = password
        self.enabled = enabled
        self.email = email
        self.sex = sex
        self.points = points
        self.incidences = incidences
    }
}
