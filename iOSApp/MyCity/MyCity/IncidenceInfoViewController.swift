//
//  IncidenceInfoViewController.swift
//  MyCity
//
//  Created by Alexandre Martinez Olmos on 16/6/15.
//  Copyright (c) 2015 BKPAMO. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation
import MobileCoreServices
import Alamofire

class IncidenceInfoViewController: UIViewController, MKMapViewDelegate, CLLocationManagerDelegate,UINavigationControllerDelegate {
    @IBOutlet weak var viewIncidence: UIView!
    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var userLabel: UILabel!
    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var townLabel: UILabel!
    @IBOutlet weak var typeLabel: UILabel!
    @IBOutlet weak var descriptionText: UITextView!
    @IBOutlet weak var mapView: MKMapView!
    
    var locationManager:CLLocationManager!
    var viewIncidenceHidden = false
    var latitudeCoordinate = ""
    var longitudeCoordinate = ""
    var incidenceAnnotation: MyCustomAnnotation = MyCustomAnnotation(c: CLLocationCoordinate2D(latitude: 16.40, longitude: -86.34), t: "", st: "", ty: 0, i: UIImage(named: "errorIconMap.png")!, u: "", d: "", dat: "", tow: "", s: 0)
    
    var annotationTitleInfo: [String] = ["Error de Anotación", "Inundación", "Dificil acceso para minusválidos", "Vegetación en mal estado", "Falta de señalización", "Acumulación de desechos", "Desprendimientos", "Falta de iluminación", "Desperfectos u objetos en calzada"]
    var annotationIconsMap: [String] = ["errorIconMap.png", "floodIconMap.png", "handicappedIconMap.png", "treeIconMap.png", "lightIconMap.png", "trashIconMap.png", "rocksroadIconMap.png", "streetlightIconMap.png", "obstacleIconMap.png"]

    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Code to make the imageView like a circle
        profileImage.layer.borderWidth=1.0
        profileImage.layer.masksToBounds = false
        profileImage.layer.borderColor = UIColor.blackColor().CGColor
        profileImage.layer.cornerRadius = 13
        profileImage.layer.cornerRadius = profileImage.frame.size.height/2
        profileImage.clipsToBounds = true
        
        zoomToIncidenceLocation(incidenceAnnotation.coordinate)
        applyPlainShadow(self.viewIncidence)
        
        if (incidenceAnnotation.status == 0) {
            statusLabel.text = "Estado: Informada"
            statusLabel.textColor = UIColor.redColor()
        } else if (incidenceAnnotation.status == 1) {
            statusLabel.text = "Estado: En Revisión"
            statusLabel.textColor = UIColor.orangeColor()
        } else {
            statusLabel.text = "Estado: Resuelta"
            statusLabel.textColor = UIColor.greenColor()
        }
        
        //The idea with the date is transform the string in a date value and then transform the date value to string with a new format better to show in the app
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        let myDate = dateFormatter.dateFromString(incidenceAnnotation.date)
        
        dateFormatter.dateFormat = "dd-MM-yyyy"
        var dateString = dateFormatter.stringFromDate(myDate!)
        dateLabel.text = "Fecha: \(dateString)"
        //////////
        
        typeLabel.text = "Tipo: \(annotationTitleInfo[incidenceAnnotation.type])"
        descriptionText.text! = incidenceAnnotation.descript!

        //The server returns the links of the user and town because of the unions in the DB. For this reaseon, whe have to request to the server for the info of these variables.
        var baseUserURL = incidenceAnnotation.user
        Alamofire.request(.GET, baseUserURL)
            .responseJSON { (request, response, data, error) in 
                if (data != nil) {
                    var dic: NSDictionary! = data as! NSDictionary
                    
                    if (dic.count > 0 && error == nil) {
                        var username = data!.valueForKey("username")
                        self.userLabel.text = "\(username!)"
                    }
                }
        }
        
        var baseTownURL = incidenceAnnotation.town
        Alamofire.request(.GET, baseTownURL)
            .responseJSON { (request, response, data, error) in
                if (data != nil) {
                    var dic: NSDictionary! = data as! NSDictionary
                    
                    if (dic.count > 0 && error == nil) {
                        var townName = data!.valueForKey("name")
                        self.townLabel.text = "\(townName!)"
                    }
                }
        }
        
        self.mapView.addAnnotation(incidenceAnnotation)
    }
    
    func mapView(mapView: MKMapView!, viewForAnnotation annotation: MKAnnotation!) -> MKAnnotationView! {
        var pinView: MKPinAnnotationView = MKPinAnnotationView(annotation: annotation, reuseIdentifier: "Custom")
        
        if (annotation is MKUserLocation) {
            //if annotation is not an MKPointAnnotation (eg. MKUserLocation),
            //return nil so map draws default view for it (eg. blue dot)...
            return nil
        }
        
        //Hacemos el cast para convertirlo a la clase MycustomAnnotation y poder acceder a todos sus atributos
        var currentAnnotation = annotation as! MyCustomAnnotation
        
        pinView.image = UIImage(named: self.annotationIconsMap[currentAnnotation.type])
        pinView.canShowCallout = true
        
        var iconView = UIImageView(image: UIImage(named: self.annotationIconsMap[currentAnnotation.type]))
        pinView.leftCalloutAccessoryView = iconView
        
        return pinView
    }
    
    @IBAction func showMenuAction(sender: AnyObject) {
        self.showSlideMenu()
    }
    
    private func showSlideMenu() {
        var position = self.viewIncidence.frame;
        
        if (viewIncidenceHidden == false) {
            viewIncidenceHidden = true
            position = CGRectMake(280,0,320,480)
            
        }
            
        else {
            viewIncidenceHidden = false
            position = CGRectMake(0,0,320,480)
        }
        
        UIView.beginAnimations(nil, context: nil)
        
        UIView.animateWithDuration(0.25) {
            self.viewIncidence.frame = position
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func locationManager(manager: CLLocationManager!, didUpdateLocations locations: [AnyObject]!) {
        let location = locations.last as! CLLocation
        let center = CLLocationCoordinate2D(latitude: location.coordinate.latitude, longitude: location.coordinate.longitude)
        let region = MKCoordinateRegion(center: center, span: MKCoordinateSpan(latitudeDelta: 0.01, longitudeDelta: 0.01))
        
        CLGeocoder().reverseGeocodeLocation(manager.location, completionHandler: {(placemarks, error)->Void in
            
            if (error != nil) {
                println("Reverse geocoder failed with error" + error.localizedDescription)
                return
            }
            
            if placemarks.count > 0 {
                let pm = placemarks[0] as! CLPlacemark
                var loc = location.coordinate
                self.latitudeCoordinate = "\(loc.latitude)"
                self.longitudeCoordinate = "\(loc.longitude)"
                
                println("")
                println("//////////////////////////////////")
                println("USER LOCATION DATA")
                println("USER LOCALITIY: " + pm.locality)
                println("USER POSTALCODE: " + pm.postalCode)
                println("USER ADMINISTRATIVE AREA: " + pm.administrativeArea)
                println("USER COUNTRY: " + pm.country)
                println("//////////////////////////////////")
            } else {
                println("Problem with the data received from geocoder")
            }
        })
    }
    
    func zoomToIncidenceLocation(c: CLLocationCoordinate2D) {
        var mapCamera = MKMapCamera(lookingAtCenterCoordinate: c, fromEyeCoordinate: c, eyeAltitude: 1000)
        mapView.setCamera(mapCamera, animated: true)
    }
    
    func applyPlainShadow(view: UIView) {
        var layer = view.layer
        
        layer.shadowColor = UIColor.blackColor().CGColor
        layer.shadowOffset = CGSize(width: 0, height: 10)
        layer.shadowOpacity = 0.4
        layer.shadowRadius = 5
    }
}
