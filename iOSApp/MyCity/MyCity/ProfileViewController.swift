//
//  ProfileViewController.swift
//  MyCity
//
//  Created by Alexandre Martinez Olmos on 17/6/15.
//  Copyright (c) 2015 BKPAMO. All rights reserved.
//

import UIKit
import Alamofire

class ProfileViewController: UIViewController {
    @IBOutlet weak var viewProfile: UIView!
    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var usernameLabel: UILabel!
    @IBOutlet weak var pointsLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var lastNameLabel: UILabel!
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var numberIncidencesLabel: UILabel!
    
    var viewProfileHidden = false

    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Code to make the imageView like a circle
        profileImage.layer.borderWidth=1.0
        profileImage.layer.masksToBounds = false
        profileImage.layer.borderColor = UIColor.blackColor().CGColor
        profileImage.layer.cornerRadius = 13
        profileImage.layer.cornerRadius = profileImage.frame.size.height/2
        profileImage.clipsToBounds = true
        
        var baseURL = "http://localhost:8080/user?username=\(userLogged)"
    
        Alamofire.request(.GET, baseURL)
            .responseJSON { (request, response, data, error) in
                println("DATA OF THE USER: \(data)")
                
                if (data != nil) {
                    var dic: NSDictionary! = data as! NSDictionary
                    
                    if (dic.count > 0 && error == nil) {
                        var id: AnyObject! = data!.valueForKey("id")
                        var email: AnyObject! = data!.valueForKey("email")
                        var username: AnyObject! = data!.valueForKey("username")
                        var points: AnyObject! = data!.valueForKey("points")
                        var name: AnyObject! = data!.valueForKey("name")
                        var lastName: AnyObject! = data!.valueForKey("lastname")
                        
                        var incidences = data!.valueForKey("incidences") as! NSArray
                        var incidencesCount = incidences.count
                        
                        self.usernameLabel.text = "\(username)"
                        self.emailLabel.text = "Email: \(email)"
                        self.pointsLabel.text = "Puntuación: \(points)"
                        self.nameLabel.text = "Nombre: \(name)"
                        self.lastNameLabel.text = "Apellido: \(lastName)"
                        self.numberIncidencesLabel.text = "Incidencias Reportadas: \(incidencesCount)"
                    }
                }
        }

        applyPlainShadow(self.viewProfile)
    }
    
    @IBAction func showMenuAction(sender: AnyObject) {
        self.showSlideMenu()
    }
    
    private func showSlideMenu() {
        var position = self.viewProfile.frame;
        
        if (viewProfileHidden == false) {
            viewProfileHidden = true
            position = CGRectMake(280,0,320,480)
        }
            
        else {
            viewProfileHidden = false
            position = CGRectMake(0,0,320,480)
        }
        
        UIView.beginAnimations(nil, context: nil)
        
        UIView.animateWithDuration(0.25) {
            self.viewProfile.frame = position
        }
    }
    
    func applyPlainShadow(view: UIView) {
        var layer = view.layer
        
        layer.shadowColor = UIColor.blackColor().CGColor
        layer.shadowOffset = CGSize(width: 0, height: 10)
        layer.shadowOpacity = 0.4
        layer.shadowRadius = 5
    }
    
    @IBAction func logOutAction(sender: AnyObject) {
        //Destroy the present session and delete the credentials in keychain
        //...
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewControllerWithIdentifier("LoginViewController") as! LoginViewController
        self.presentViewController(vc, animated: true, completion: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}
