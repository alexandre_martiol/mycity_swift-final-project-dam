//
//  IncidenceTableViewCell.swift
//  MyCity
//
//  Created by Alexandre Martinez Olmos on 16/6/15.
//  Copyright (c) 2015 BKPAMO. All rights reserved.
//

import UIKit

class IncidenceTableViewCell: UITableViewCell {
    @IBOutlet weak var coverImage: UIImageView!
    @IBOutlet weak var typeIconImage: UIImageView!
    @IBOutlet weak var typeLabel: UILabel!
    @IBOutlet weak var statusLabel: UILabel!
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
