//
//  IncidencesTableViewController.swift
//  MyCity
//
//  Created by Alexandre Martinez Olmos on 16/6/15.
//  Copyright (c) 2015 BKPAMO. All rights reserved.
//

import UIKit
import Alamofire
import CoreLocation

class IncidencesTableViewController: UITableViewController, UITableViewDelegate, UITableViewDataSource
{
    @IBOutlet var table: UITableView!
    var incidencesArray: [NSDictionary] = []
    var isCurrentUserRequest = false
    var currentUserId = 0
    
    var annotationIconsMap: [String] = ["errorIcon.png", "floodIcon.png", "handicappedIcon.png", "treeIcon.png", "lightIcon.png", "trashIcon.png", "rocksroadIcon.png", "streetlightIcon.png", "obstacleIcon.png"]
    var annotationTitleInfo: [String] = ["Error de Anotación", "Inundación", "Dificil acceso para minusválidos", "Vegetación en mal estado", "Falta de señalización", "Acumulación de desechos", "Desprendimientos", "Falta de iluminación", "Desperfectos u objetos en calzada"]

    override func viewDidLoad() {
        super.viewDidLoad()
        
        table.delegate = self
        table.dataSource = self
        
        self.refreshTable()
    }
    
    //We use this view to show all the incidences and the incidences of the logged user. For this reason we have to requests depending on the origin.
    func refreshTable() {
        if (isCurrentUserRequest == false) {
            var baseURL = "http://localhost:8080/incidences"
            
            Alamofire.request(.GET, baseURL)
                .responseJSON { (request, response, data, error) in
                    
                    if (data != nil) {
                        var dic: NSDictionary! = data as! NSDictionary
                        
                        if (dic.count > 0 && error == nil) {
                            var embedded  = data!.valueForKey("_embedded") as! NSDictionary
                            self.incidencesArray = embedded.valueForKey("incidences") as! [NSDictionary]
                            
                            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                                self.table.reloadData()
                            })
                        }
                    }
            }
        }
        
        else if (isCurrentUserRequest == true) {
            var baseURL = "http://localhost:8080/user?username=\(userLogged)"
            
            Alamofire.request(.GET, baseURL)
                .responseJSON { (request, response, data, error) in
                    
                    if (data != nil) {
                        var dic: NSDictionary! = data as! NSDictionary
                       
                        if (dic.count > 0 && error == nil) {
                            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                                var id: AnyObject! = data!.valueForKey("id")
                                self.currentUserId = id.integerValue
                                
                                baseURL = "http://localhost:8080/incidence?user=\(self.currentUserId)"
                                
                                Alamofire.request(.GET, baseURL)
                                    .responseJSON { (request, response, data, error) in
                                        
                                        if (data != nil) {
                                            var dic: NSArray! = data as! NSArray
                                            
                                            if (dic.count > 0 && error == nil) {
                                                self.incidencesArray = dic as! [NSDictionary]
                                                
                                                dispatch_async(dispatch_get_main_queue(), { () -> Void in
                                                    self.table.reloadData()
                                                })
                                            }
                                        }
                                }
                            })
                        }
                    }
            }
            
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Potentially incomplete method implementation.
        // Return the number of sections.
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete method implementation.
        // Return the number of rows in the section.
        return incidencesArray.count
    }

    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell: IncidenceTableViewCell = tableView.dequeueReusableCellWithIdentifier("incidenceCell") as! IncidenceTableViewCell!

        // Configure the cell...
        var currentIncidence = incidencesArray[indexPath.row] as NSDictionary
        var typeIncindence: Int! = currentIncidence["type"]!.integerValue
        var statusIncidence: Int! = currentIncidence["status"]!.integerValue
        
        if (statusIncidence == 0) {
            cell.statusLabel.text = "Estado: Informada"
            cell.statusLabel.textColor = UIColor.redColor()
        } else if (statusIncidence == 1) {
            cell.statusLabel.text = "Estado: En Revisión"
            cell.statusLabel.textColor = UIColor.orangeColor()
        } else {
            cell.statusLabel.text = "Estado: Resuelta"
            cell.statusLabel.textColor = UIColor.greenColor()
        }
        
        cell.typeIconImage.image = UIImage(named: self.annotationIconsMap[typeIncindence])
        cell.typeLabel.text = "Tipo: \(self.annotationTitleInfo[typeIncindence])"
        
        return cell
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        var currentIncidence = self.incidencesArray[indexPath.row] as NSDictionary
        var currentIncidenceId: AnyObject! = currentIncidence["id"]
        
        var latitude = currentIncidence["latitudeCoordinate"]!.doubleValue
        var longitude = currentIncidence["longitudeCoordinate"]!.doubleValue
        var incidenceLocation:CLLocationCoordinate2D = CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
        
        var typeAnnotation: Int = currentIncidence["type"]!.integerValue
        var titleAnnotation: AnyObject! = self.annotationTitleInfo[typeAnnotation]
        
        var statusAnnotation: Int = currentIncidence["status"]!.integerValue
        var subtitleAnnotation = ""
        
        if (statusAnnotation == 0) {
            subtitleAnnotation = "Estado: Informada"
        } else if (statusAnnotation == 1) {
            subtitleAnnotation = "Estado: En Revisión"
        } else {
            subtitleAnnotation = "Estado: Resuelta"
        }
        
        var descriptionAnnotation: AnyObject! = currentIncidence["description"]
        var dateAnnotation: AnyObject!
        var userLink: AnyObject!
        var townLink: AnyObject!
        
        //The information sent by the server is different depending on the request origin. So we have to separate it in order to obtain the info correctly.
        if (isCurrentUserRequest == false) {
            dateAnnotation = currentIncidence["date"]
            
            var links = currentIncidence["_links"] as! NSDictionary
            
            var userAnnotation = links["user"] as! NSDictionary
            userLink = userAnnotation["href"]
            
            var townAnnotation = links["town"] as! NSDictionary
            townLink = townAnnotation["href"]
        }
        
        else {
            var dateAnnotationValue = currentIncidence["date"]!.doubleValue
            let dateFormatter = NSDateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
            let myDate = NSDate().dateByAddingTimeInterval(dateAnnotationValue/1000 as NSTimeInterval)
            dateAnnotation = dateFormatter.stringFromDate(myDate)
            
            userLink = "http://localhost:8080/incidences/\(currentIncidenceId)/user"
            
            townLink = "http://localhost:8080/incidences/\(currentIncidenceId)/town"
        }
        ////////
        
        var anotation = MyCustomAnnotation(c: incidenceLocation, t: "\(titleAnnotation!)", st: subtitleAnnotation, ty: typeAnnotation, i: UIImage(named: self.annotationIconsMap[typeAnnotation])!, u: "\(userLink)", d: "\(descriptionAnnotation)", dat: "\(dateAnnotation)", tow: "\(townLink)", s: statusAnnotation)
        
        var storyboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        var vc : IncidenceInfoViewController = storyboard.instantiateViewControllerWithIdentifier("IncidenceInfoViewController") as! IncidenceInfoViewController
        
        vc.incidenceAnnotation = anotation
        
        self.presentViewController(vc, animated: true, completion: nil)
    }
}
