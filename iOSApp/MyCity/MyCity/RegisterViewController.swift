//
//  RegisterViewController.swift
//
//
//  Created by Alexandre Martinez Olmos on 18/2/15.
//
//

import UIKit
import MobileCoreServices
import Alamofire

class RegisterViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UIPickerViewDelegate {
    @IBOutlet weak var userImage: UIImageView!
    @IBOutlet weak var nameText: UITextField!
    @IBOutlet weak var lastnameText: UITextField!
    @IBOutlet weak var emailText: UITextField!
    @IBOutlet weak var usernameText: UITextField!
    @IBOutlet weak var passText: UITextField!
    @IBOutlet weak var repeatpassText: UITextField!
    @IBOutlet weak var pickerSex: UIPickerView!
    @IBOutlet weak var chooseSexButton: UIButton!
    
    var newMedia: Bool?
    var sexArray = ["Hombre", "Mujer"]
    //sex: 0 = male / 1 = female
    var sex = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    ///////////////////////////////////////////////////////////////////////////////////////////////////////
    //START FUNCTIONS CONTROLL USER IMAGE
    //Tutorial: http://www.techotopia.com/index.php/An_Example_Swift_iOS_8_iPhone_Camera_Application
    @IBAction func useCamera(sender: AnyObject) {
        if UIImagePickerController.isSourceTypeAvailable(
            UIImagePickerControllerSourceType.Camera) {
                
                let imagePicker = UIImagePickerController()
                
                imagePicker.delegate = self
                imagePicker.sourceType =
                    UIImagePickerControllerSourceType.Camera
                imagePicker.mediaTypes = [kUTTypeImage as NSString]
                imagePicker.allowsEditing = false
                
                self.presentViewController(imagePicker, animated: true,
                    completion: nil)
                newMedia = true
        }
            
        else if UIImagePickerController.isSourceTypeAvailable(
            UIImagePickerControllerSourceType.SavedPhotosAlbum) {
                let imagePicker = UIImagePickerController()
                
                imagePicker.delegate = self
                imagePicker.sourceType = UIImagePickerControllerSourceType.PhotoLibrary
                imagePicker.mediaTypes = [kUTTypeImage as NSString]
                imagePicker.allowsEditing = false
                self.presentViewController(imagePicker, animated: true, completion: nil)
                newMedia = false
        }
    }
    
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [NSObject : AnyObject]) {
        
        let mediaType = info[UIImagePickerControllerMediaType] as! NSString
        
        self.dismissViewControllerAnimated(true, completion: nil)
        
        if mediaType.isEqualToString(kUTTypeImage as! String) {
            let image = info[UIImagePickerControllerOriginalImage]
                as! UIImage
            
            userImage.image = image
            
            if (newMedia == true) {
                UIImageWriteToSavedPhotosAlbum(image, self,
                    "image:didFinishSavingWithError:contextInfo:", nil)
            } else if mediaType.isEqualToString(kUTTypeMovie as! String) {
                // Code to support video here
            }
            
        }
    }
    
    func image(image: UIImage, didFinishSavingWithError error: NSErrorPointer, contextInfo:UnsafePointer<Void>) {
        
        if error != nil {
            let alert = UIAlertController(title: "Save Failed",
                message: "Failed to save image",
                preferredStyle: UIAlertControllerStyle.Alert)
            
            let cancelAction = UIAlertAction(title: "OK",
                style: .Cancel, handler: nil)
            
            //alert.view.backgroundColor = UIColor(red:0, green:(100.0/255.0), blue:0, alpha:1.0)
            //alert.view.tintColor = UIColor.whiteColor()
            
            alert.addAction(cancelAction)
            self.presentViewController(alert, animated: true,
                completion: nil)
        }
    }
    
    func imagePickerControllerDidCancel(picker: UIImagePickerController) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    //END FUNCTIONS CONTROLL USER IMAGE
    ///////////////////////////////////////////////////////////////////////////////////////////////////////
    
    @IBAction func registerAction(sender: AnyObject) {
        if(!checkEmptyFields()) {
            let parameters = [
                "email": emailText.text,
                "enabled": 1,
                "lastname": lastnameText.text,
                "name": nameText.text,
                "password": passText.text,
                "points": 0,
                "sex": sex,
                "username" : usernameText.text]
            var baseURL = "http://localhost:8080/user/registration"
         
            Alamofire.request(.POST, baseURL, parameters: parameters as? [String : AnyObject], encoding: .JSON)
            
            print("USER WITH USERNAME: \(usernameText.text) HAS BEEN REGISTERED")
            
            var storyboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            var vc : MapViewController = storyboard.instantiateViewControllerWithIdentifier("MapViewController") as! MapViewController
            
            self.presentViewController(vc, animated: true, completion: nil)
        }
        
        else {
            print("USER NOT REGISTERED")
        }
    }
    
    func checkEmptyFields() -> Bool{
        if (nameText.text.isEmpty ||
            lastnameText.text.isEmpty ||
            emailText.text.isEmpty ||
            usernameText.text.isEmpty ||
            passText.text.isEmpty ||
            repeatpassText.text.isEmpty) {
                let alertController = UIAlertController(title: "Campos vacíos", message:
                    "Debe rellenar todos los campos", preferredStyle: UIAlertControllerStyle.Alert)
                alertController.addAction(UIAlertAction(title: "Aceptar", style: UIAlertActionStyle.Default,handler: nil))
                
                self.presentViewController(alertController, animated: true, completion: nil)
                
                print("THERE ARE EMPTY FIELDS")
                return true
                
        } else if (passText.text != repeatpassText.text) {
                let alertController = UIAlertController(title: "Contraseña incorrecta", message:
                    "La contraseña debe coincidir", preferredStyle: UIAlertControllerStyle.Alert)
                alertController.addAction(UIAlertAction(title: "Aceptar", style: UIAlertActionStyle.Default,handler: nil))
            
                self.presentViewController(alertController, animated: true, completion: nil)
            
                print("PASSWORDS ARE NOT THE SAME")
                return true
        }
        
        return false
    }
    
    @IBAction func chooseSexAction(sender: AnyObject) {
        let alertController = UIAlertController(title: "Choose sex", message: "What is your sex?", preferredStyle: .Alert)
        
        let maleAction = UIAlertAction(title: "Male", style: .Default) { (action) in
            self.sex = 0
            self.chooseSexButton.titleLabel?.text = "Male"
        }
        alertController.addAction(maleAction)
        
        let femaleAction = UIAlertAction(title: "Female", style: .Default) { (action) in
            self.sex = 1
            self.chooseSexButton.titleLabel?.text = "Female"
        }
        alertController.addAction(femaleAction)
        
        self.presentViewController(alertController, animated: true, completion: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
