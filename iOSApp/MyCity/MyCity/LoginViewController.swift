//
//  ViewController.swift
//  MyCity
//
//  Created by AlexM on 12/02/15.
//  Copyright (c) 2015 BKPAMO. All rights reserved.
//

import UIKit
import Alamofire

class LoginViewController: UIViewController {
    @IBOutlet weak var usernameText: UITextField!
    @IBOutlet weak var passText: UITextField!

    override func viewDidLoad() {
        super.viewDidLoad()
    }


    @IBAction func loginAction(sender: AnyObject) {
        if(!checkEmptyFields()) {
            //This url goes directly to Spring Data
            var baseURL = "http://localhost:8080/users/search/findByUsername?username=\(usernameText.text)"

            Alamofire.request(.GET, baseURL).authenticate(user: usernameText.text, password: passText.text).responseJSON { (request, response, data, error) in
                println("DATA OF THE USER: \(data)")

                if (data != nil) {
                    var dic: NSDictionary! = data as! NSDictionary
                    
                    if (dic.count > 0 && error == nil) {
                        println("THE USER EXISTS AND LOGS IN")
                        var embedded  = data!.valueForKey("_embedded") as! NSDictionary
                        var users = embedded.valueForKey("users") as! [NSDictionary]
                        var email: AnyObject! = users[0]["email"]
                        var username: AnyObject! = users[0]["username"]
                        userLogged = "\(username)"
                        
                        println(email)
                        
                        var storyboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                        var vc : MapViewController = storyboard.instantiateViewControllerWithIdentifier("MapViewController") as! MapViewController
                        
                        self.presentViewController(vc, animated: true, completion: nil)
                    }
                        
                    else {
                        println("THE USER DOESN'T EXISTS")
                        
                        let alertController = UIAlertController(title: "Imposible entrar", message:
                            "Compruebe que la información es correcta", preferredStyle: UIAlertControllerStyle.Alert)
                        alertController.addAction(UIAlertAction(title: "Aceptar", style: UIAlertActionStyle.Default,handler: nil))
                        
                        self.presentViewController(alertController, animated: true, completion: nil)
                    }
                }
                
                else {
                    let alertController = UIAlertController(title: "Imposible entrar", message:
                        "Compruebe que la información es correcta", preferredStyle: UIAlertControllerStyle.Alert)
                    alertController.addAction(UIAlertAction(title: "Aceptar", style: UIAlertActionStyle.Default,handler: nil))
                    
                    self.presentViewController(alertController, animated: true, completion: nil)
                }
                
            }
        }
    }
    
    func checkEmptyFields() -> Bool{
        if (usernameText.text.isEmpty ||
            passText.text.isEmpty) {
                let alertController = UIAlertController(title: "Campos vacíos", message:
                    "Debe rellenar todos los campos", preferredStyle: UIAlertControllerStyle.Alert)
                alertController.addAction(UIAlertAction(title: "Aceptar", style: UIAlertActionStyle.Default,handler: nil))
                
                self.presentViewController(alertController, animated: true, completion: nil)
                
                println("EMPTY FIELDS IN LOG IN")
                return true
        } else {return false}
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

