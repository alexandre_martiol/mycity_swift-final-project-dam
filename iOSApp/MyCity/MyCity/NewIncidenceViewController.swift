//
//  NewIncidenceViewController.swift
//  MyCity
//
//  Created by Alexandre Martinez Olmos on 23/2/15.
//  Copyright (c) 2015 BKPAMO. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation
import MobileCoreServices
import Alamofire

class NewIncidenceViewController: UIViewController, MKMapViewDelegate, CLLocationManagerDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    @IBOutlet weak var descriptionText: UITextView!
    @IBOutlet weak var incidenceTypeLabel: UILabel!
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var incidenceImage: UIImageView!
    @IBOutlet weak var incidenceImage2: UIImageView!
    var firstImageChoosed = false
    var secondImageChoosed = false
    
    @IBOutlet weak var viewIncidence: UIView!
    var viewIncidenceHidden = false
    
    var locationManager:CLLocationManager!
    
    var newMedia: Bool?
    var incidenceType = 0
    var latitudeCoordinate = ""
    var longitudeCoordinate = ""
    var userInfo = ""

    override func viewDidLoad() {
        super.viewDidLoad()
        
        locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestAlwaysAuthorization()
        self.mapView.showsUserLocation = true
        locationManager.startUpdatingLocation()
        
        zoomToUserLocation()
        applyPlainShadow(self.viewIncidence)
    }

    @IBAction func showMenuAction(sender: AnyObject) {
        self.showSlideMenu()
    }
    
    private func showSlideMenu() {
        var position = self.viewIncidence.frame;
        
        if (viewIncidenceHidden == false) {
            viewIncidenceHidden = true
            position = CGRectMake(280,0,320,480)
            
        }
            
        else {
            viewIncidenceHidden = false
            position = CGRectMake(0,0,320,480)
        }
        
        UIView.beginAnimations(nil, context: nil)
        
        UIView.animateWithDuration(0.25) {
            self.viewIncidence.frame = position
        }
    }
    
    @IBAction func typeIncidenceAction(sender: UIButton) {
        
        switch sender.tag {
        case 1:
            self.incidenceTypeLabel.text = "Inundación"
            incidenceType = 1
        case 2:
            self.incidenceTypeLabel.text = "Dificil acceso para minusválidos"
            incidenceType = 2
        case 3:
            self.incidenceTypeLabel.text = "Vegetación en mal estado"
            incidenceType = 3
        case 4:
            self.incidenceTypeLabel.text = "Falta de señalización"
            incidenceType = 4
        case 5:
            self.incidenceTypeLabel.text = "Acumulación de desechos"
            incidenceType = 5
        case 6:
            self.incidenceTypeLabel.text = "Desprendimientos"
            incidenceType = 6
        case 7:
            self.incidenceTypeLabel.text = "Falta de iluminación"
            incidenceType = 7
        case 8:
            self.incidenceTypeLabel.text = "Desperfectos u objetos en calzada"
            incidenceType = 8
        default:
            self.incidenceTypeLabel.text = "Ninguna incidencia seleccionada"
            incidenceType = 0
        }
        
    }
    
    func locationManager(manager: CLLocationManager!, didUpdateLocations locations: [AnyObject]!) {
        let location = locations.last as! CLLocation
        let center = CLLocationCoordinate2D(latitude: location.coordinate.latitude, longitude: location.coordinate.longitude)
        let region = MKCoordinateRegion(center: center, span: MKCoordinateSpan(latitudeDelta: 0.01, longitudeDelta: 0.01))
        
        CLGeocoder().reverseGeocodeLocation(manager.location, completionHandler: {(placemarks, error)->Void in
            
            if (error != nil) {
                println("Reverse geocoder failed with error" + error.localizedDescription)
                return
            }
            
            if placemarks.count > 0 {
                let pm = placemarks[0] as! CLPlacemark
                var loc = location.coordinate
                self.latitudeCoordinate = "\(loc.latitude)"
                self.longitudeCoordinate = "\(loc.longitude)"
                    
                println("")
                println("//////////////////////////////////")
                println("USER LOCATION DATA")
                println("USER LOCALITIY: " + pm.locality)
                println("USER POSTALCODE: " + pm.postalCode)
                println("USER ADMINISTRATIVE AREA: " + pm.administrativeArea)
                println("USER COUNTRY: " + pm.country)
                println("//////////////////////////////////")
            } else {
                println("Problem with the data received from geocoder")
            }
        })
    }
    
    func zoomToUserLocation() {
        self.mapView.setUserTrackingMode(MKUserTrackingMode.Follow, animated: true);
    }
    
    ///////////////////////////////////////////////////////////////////////////////////////////////////////
    //START FUNCTIONS CONTROLL USER IMAGE
    //Tutorial: http://www.techotopia.com/index.php/An_Example_Swift_iOS_8_iPhone_Camera_Application
    @IBAction func useCamera(sender: AnyObject) {
        if UIImagePickerController.isSourceTypeAvailable(
            UIImagePickerControllerSourceType.Camera) {
                
                let imagePicker = UIImagePickerController()
                
                imagePicker.delegate = self
                imagePicker.sourceType =
                    UIImagePickerControllerSourceType.Camera
                imagePicker.mediaTypes = [kUTTypeImage as NSString]
                imagePicker.allowsEditing = false
                
                self.presentViewController(imagePicker, animated: true,
                    completion: nil)
                newMedia = true
        }
            
        else if UIImagePickerController.isSourceTypeAvailable(
            UIImagePickerControllerSourceType.SavedPhotosAlbum) {
                let imagePicker = UIImagePickerController()
                
                imagePicker.delegate = self
                imagePicker.sourceType = UIImagePickerControllerSourceType.PhotoLibrary
                imagePicker.mediaTypes = [kUTTypeImage as NSString]
                imagePicker.allowsEditing = false
                self.presentViewController(imagePicker, animated: true, completion: nil)
                newMedia = false
        }
    }
    
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [NSObject : AnyObject]) {
        
        let mediaType = info[UIImagePickerControllerMediaType] as! NSString
        
        self.dismissViewControllerAnimated(true, completion: nil)
        
        if mediaType.isEqualToString(kUTTypeImage as! String) {
            let image = info[UIImagePickerControllerOriginalImage]
                as! UIImage
            
            //incidenceImage.image = image
            
            if (firstImageChoosed == false) {
                incidenceImage.image = image
                firstImageChoosed = true
            }
            
            else if (secondImageChoosed == false){
                incidenceImage2.image = image
                secondImageChoosed = true
            }
            
            else {
                let alert = UIAlertController(title: "No más imágenes",
                    message: "¡Ya has subido dos imágenes!",
                    preferredStyle: UIAlertControllerStyle.Alert)
                
                let cancelAction = UIAlertAction(title: "Aceptar",
                    style: .Cancel, handler: nil)
                
                alert.addAction(cancelAction)
                self.presentViewController(alert, animated: true,
                    completion: nil)
            }
            
            if (newMedia == true) {
                UIImageWriteToSavedPhotosAlbum(image, self,
                    "image:didFinishSavingWithError:contextInfo:", nil)
            } else if mediaType.isEqualToString(kUTTypeMovie as! String) {
                // Code to support video here
            }
            
        }
    }
    
    func image(image: UIImage, didFinishSavingWithError error: NSErrorPointer, contextInfo:UnsafePointer<Void>) {
        
        if error != nil {
            let alert = UIAlertController(title: "Save Failed",
                message: "Failed to save image",
                preferredStyle: UIAlertControllerStyle.Alert)
            
            let cancelAction = UIAlertAction(title: "OK",
                style: .Cancel, handler: nil)
            
            alert.addAction(cancelAction)
            self.presentViewController(alert, animated: true,
                completion: nil)
        }
    }
    
    func imagePickerControllerDidCancel(picker: UIImagePickerController) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    //END FUNCTIONS CONTROLL USER IMAGE
    ///////////////////////////////////////////////////////////////////////////////////////////////////////
    
    func applyPlainShadow(view: UIView) {
        var layer = view.layer
        
        layer.shadowColor = UIColor.blackColor().CGColor
        layer.shadowOffset = CGSize(width: 0, height: 10)
        layer.shadowOpacity = 0.4
        layer.shadowRadius = 5
    }
    
    @IBAction func cancelAction(sender: AnyObject) {
        let alertController = UIAlertController(title: "Descartar Incidencia", message:
            "¿Estás seguro de que quieres descartar la incidencia?", preferredStyle: UIAlertControllerStyle.Alert)
        
        alertController.addAction(UIAlertAction(title: "Descartar", style: .Destructive) { (action) in
            var storyboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            var vc : MapViewController = storyboard.instantiateViewControllerWithIdentifier("MapViewController") as! MapViewController
            
            self.presentViewController(vc, animated: true, completion: nil)
        })
        
        alertController.addAction(UIAlertAction(title: "Cancelar", style: .Cancel, handler:nil))
        
        self.presentViewController(alertController, animated: true, completion: nil)
    }
    
    @IBAction func uploadIncidence(sender: AnyObject) {
        if (incidenceType > 0) {
            //We prepare the date with the Spain time zone
            let date = NSDate();
            var formatter = NSDateFormatter();
            formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";
            let defaultTimeZoneStr = formatter.stringFromDate(date);
            formatter.timeZone = NSTimeZone(abbreviation: "CEST");
            let cestDate = formatter.stringFromDate(date);
            
            //Introduce the name of the user in the address var because it is not used and the Back End spects an User in the user var.
            let parameters = [
            "description": descriptionText.text,
            "address": userLogged,
            "date": cestDate,
            "status": 0,
            "latitudeCoordinate": self.latitudeCoordinate,
            "longitudeCoordinate": self.longitudeCoordinate,
            "type": incidenceType]
            
            var baseURL = "http://localhost:8080/incidence/new"
            Alamofire.request(.POST, baseURL, parameters: parameters as? [String : AnyObject], encoding: .JSON).progress { (bytesRead, totalBytesRead, totalBytesExpectedToRead) -> Void in
                println("ENTER .PROGRESSS")
                println("\(totalBytesRead) of \(totalBytesExpectedToRead)")
                }
                .responseJSON { (_, _, mydata, _) in
                    println(mydata)
            }
            
            println("THE INCIDENCE HAS BEEN UPLOADED")
            
            var storyboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            var vc : MapViewController = storyboard.instantiateViewControllerWithIdentifier("MapViewController") as! MapViewController
            
            self.presentViewController(vc, animated: true, completion: nil)
        }
        
        else {
            let alertController = UIAlertController(title: "Tipo de incidencia no seleccionado", message:
                "Debe elegir un tipo de incidencia para informar", preferredStyle: UIAlertControllerStyle.Alert)
            alertController.addAction(UIAlertAction(title: "Aceptar", style: UIAlertActionStyle.Default,handler: nil))
            
            self.presentViewController(alertController, animated: true, completion: nil)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
