//
//  MapViewController.swift
//  MyCity
//
//  Created by AlexM on 19/02/15.
//  Copyright (c) 2015 BKPAMO. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation
import Alamofire

class MapViewController: UIViewController, MKMapViewDelegate, CLLocationManagerDelegate, UIGestureRecognizerDelegate {
    @IBOutlet weak var usernameText: UILabel!
    @IBOutlet weak var pointsText: UILabel!
    @IBOutlet weak var incidenceResumeText: UILabel!
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var containerMenu: UIView!
    
    var locationManager:CLLocationManager!
    let recognizer = UIPanGestureRecognizer()
    
    @IBOutlet weak var navigationBarView: UIView!
    @IBOutlet weak var titleButtonNavigationBar: UIButton!
    
    //The trick for a slide menu is creating 2 views. One is the normal view and another, behind the first, is the MenuView. The idea is simple, displace the first view to show the MenuView.
    @IBOutlet weak var viewMap: UIView!
    var viewMapHidden = false
    
    //Outlets of MenuView
    @IBOutlet weak var userImage: UIImageView!
    
    var incidencesArray: [Incidence] = []
    var annotationIconsMap: [String] = ["errorIconMap.png", "floodIconMap.png", "handicappedIconMap.png", "treeIconMap.png", "lightIconMap.png", "trashIconMap.png", "rocksroadIconMap.png", "streetlightIconMap.png", "obstacleIconMap.png"]
    var annotationTitleInfo: [String] = ["Error de Anotación", "Inundación", "Dificil acceso para minusválidos", "Vegetación en mal estado", "Falta de señalización", "Acumulación de desechos", "Desprendimientos", "Falta de iluminación", "Desperfectos u objetos en calzada"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestAlwaysAuthorization()
        self.mapView.showsUserLocation = true
        locationManager.startUpdatingLocation()
        
        zoomToUserLocation()
        applyPlainShadow(self.viewMap)
        
        //Instantiate methods to recongize gestures.
        //In this view, the MapView doesn't allow to use gestures.
        var swipeRight = UISwipeGestureRecognizer(target: self, action: "respondToSwipeGesture:")
        swipeRight.direction = UISwipeGestureRecognizerDirection.Right
        self.view.addGestureRecognizer(swipeRight)
        
        var swipeLeft = UISwipeGestureRecognizer(target: self, action: "respondToSwipeGesture:")
        swipeLeft.direction = UISwipeGestureRecognizerDirection.Left
        self.view.addGestureRecognizer(swipeRight)
        
        var swipeDown = UISwipeGestureRecognizer(target: self, action: "respondToSwipeGesture:")
        swipeDown.direction = UISwipeGestureRecognizerDirection.Down
        self.view.addGestureRecognizer(swipeDown)
        
        var swipeUp = UISwipeGestureRecognizer(target: self, action: "respondToSwipeGesture:")
        swipeUp.direction = UISwipeGestureRecognizerDirection.Up
        self.view.addGestureRecognizer(swipeDown)
    }
    
    func locationManager(manager: CLLocationManager!, didUpdateLocations locations: [AnyObject]!) {
        let location = locations.last as! CLLocation
        let center = CLLocationCoordinate2D(latitude: location.coordinate.latitude, longitude: location.coordinate.longitude)
        let region = MKCoordinateRegion(center: center, span: MKCoordinateSpan(latitudeDelta: 0.01, longitudeDelta: 0.01))
        
        CLGeocoder().reverseGeocodeLocation(manager.location, completionHandler: {(placemarks, error)->Void in
            
            if (error != nil) {
                println("Reverse geocoder failed with error" + error.localizedDescription)
                return
            }
            
            if placemarks.count > 0 {
                let pm = placemarks[0] as! CLPlacemark
                
                //This change the title of the navigation bar to the name of current location of the user
                self.titleButtonNavigationBar.setTitle("\(pm.locality)", forState: UIControlState.Normal)
                
                println("")
                println("//////////////////////////////////")
                println("USER LOCATION DATA")
                println("USER LOCALITIY: " + pm.locality)
                println("USER POSTALCODE: " + pm.postalCode)
                println("USER ADMINISTRATIVE AREA: " + pm.administrativeArea)
                println("USER COUNTRY: " + pm.country)
                println("//////////////////////////////////")
            } else {
                println("Problem with the data received from geocoder")
            }
        })
        
        var baseURL = "http://localhost:8080/incidences"
        
        Alamofire.request(.GET, baseURL)
            .responseJSON { (request, response, data, error) in
                //println("DATA OF THE INCIDENCES: \(data)")
                
                if (data != nil) {
                    var dic: NSDictionary! = data as! NSDictionary
                    
                    if (dic.count > 0 && error == nil) {
                        var embedded  = data!.valueForKey("_embedded") as! NSDictionary
                        var incidences = embedded.valueForKey("incidences") as! [NSDictionary]
                        var numIncidences = incidences.count

                        //We have to wait for the server response in order tho show the info correctly
                        dispatch_async(dispatch_get_main_queue()) {
                            for var i = 0; i < numIncidences; i++
                            {
                                var latitude = incidences[i]["latitudeCoordinate"]!.doubleValue
                                var longitude = incidences[i]["longitudeCoordinate"]!.doubleValue
                                var incidenceLocation:CLLocationCoordinate2D = CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
                                
                                var typeAnnotation: Int = incidences[i]["type"]!.integerValue
                                var titleAnnotation: AnyObject! = self.annotationTitleInfo[typeAnnotation]
                                
                                var statusAnnotation: Int = incidences[i]["status"]!.integerValue
                                var subtitleAnnotation = ""
                                
                                if (statusAnnotation == 0) {
                                    subtitleAnnotation = "Estado: Informada"
                                } else if (statusAnnotation == 1) {
                                    subtitleAnnotation = "Estado: En Revisión"
                                } else {
                                    subtitleAnnotation = "Estado: Resuelta"
                                }
                                
                                var descriptionAnnotation: AnyObject! = incidences[i]["description"]
                                var dateAnnotation: AnyObject! = incidences[i]["date"]
                                
                                var links = incidences[i]["_links"] as! NSDictionary
                                
                                var userAnnotation = links["user"] as! NSDictionary
                                var userLink: AnyObject! = userAnnotation["href"]
                                
                                var townAnnotation = links["town"] as! NSDictionary
                                var townLink: AnyObject! = townAnnotation["href"]
                                
                                //We create an annotation with all the info of the incidence because the IncidenceInfoViewController class will work with an annotation like it would be an incidence
                                var anotation = MyCustomAnnotation(c: incidenceLocation, t: "\(titleAnnotation!)", st: subtitleAnnotation, ty: typeAnnotation, i: UIImage(named: self.annotationIconsMap[typeAnnotation])!, u: "\(userLink)", d: "\(descriptionAnnotation)", dat: "\(dateAnnotation)", tow: "\(townLink)", s: statusAnnotation)
                                
                                self.mapView.addAnnotation(anotation)
                            }
                        }
                        
                    }
                }
        }
        
    }
    
    func mapView(mapView: MKMapView!, viewForAnnotation annotation: MKAnnotation!) -> MKAnnotationView! {
        var pinView: MKPinAnnotationView = MKPinAnnotationView(annotation: annotation, reuseIdentifier: "Custom")
        
        if (annotation is MKUserLocation) {
            //if annotation is not an MKPointAnnotation (eg. MKUserLocation),
            //return nil so map draws default view for it (eg. blue dot)...
            return nil
        }
        
        //Hacemos el cast para convertirlo a la clase MyCustomAnnotation y poder acceder a todos sus atributos
        var currentAnnotation = annotation as! MyCustomAnnotation
        
        pinView.image = UIImage(named: self.annotationIconsMap[currentAnnotation.type])
        pinView.canShowCallout = true
        
        
        var rightButton: UIButton = UIButton.buttonWithType(UIButtonType.DetailDisclosure) as! UIButton
        pinView.rightCalloutAccessoryView = rightButton
        
        var iconView = UIImageView(image: UIImage(named: self.annotationIconsMap[currentAnnotation.type]))
        pinView.leftCalloutAccessoryView = iconView
        return pinView
    }
    
    //We present IncidenceInfoViewController sending the incidence like an annotation
    func mapView(mapView: MKMapView!, annotationView view: MKAnnotationView!, calloutAccessoryControlTapped control: UIControl!) -> Void {
        var annotation = view.annotation as! MyCustomAnnotation
        
        
        var storyboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        var vc : IncidenceInfoViewController = storyboard.instantiateViewControllerWithIdentifier("IncidenceInfoViewController") as! IncidenceInfoViewController
        
        vc.incidenceAnnotation = annotation
        
        self.presentViewController(vc, animated: true, completion: nil)
    }
    
    func zoomToUserLocation() {
        self.mapView.setUserTrackingMode(MKUserTrackingMode.Follow, animated: true);
    }

    //When we click on the title we zoom in
    @IBAction func titleNavigationBarAction(sender: AnyObject) {
        self.zoomToUserLocation()
    }
    
    
    @IBAction func showMenuAction(sender: AnyObject) {
        self.showSlideMenu()
    }
    
    private func showSlideMenu() {
        var position = self.viewMap.frame;
        
        if (viewMapHidden == false) {
            viewMapHidden = true
            position = CGRectMake(280,0,320,480)
            
        }
            
        else {
            viewMapHidden = false
            position = CGRectMake(0,0,320,480)
        }
        
        UIView.beginAnimations(nil, context: nil)
        
        UIView.animateWithDuration(0.25) {
            self.viewMap.frame = position
        }
    }
    
    //aesthetic view for the transition of the slide menu
    func applyPlainShadow(view: UIView) {
        var layer = view.layer
        
        layer.shadowColor = UIColor.blackColor().CGColor
        layer.shadowOffset = CGSize(width: 0, height: 10)
        layer.shadowOpacity = 0.4
        layer.shadowRadius = 5
    }
    
    func respondToSwipeGesture(gesture: UIGestureRecognizer) {
        
        if let swipeGesture = gesture as? UISwipeGestureRecognizer {
            
            switch swipeGesture.direction {
            case UISwipeGestureRecognizerDirection.Right:
                self.showSlideMenu()
            case UISwipeGestureRecognizerDirection.Down:
                println("Swiped down")
            default:
                break
            }
        }
    }
    
    @IBAction func goToMapAction(sender: AnyObject) {
        //Update all the incidences and present in map
    }
}
